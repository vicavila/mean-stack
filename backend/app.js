const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


mongoose.connect("mongodb+srv://vavila:5lp2646l1T71l3Rx@cluster0-13sus.mongodb.net/mean-stack?retryWrites=true&w=majority",
{useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, }
).then(() => {
    console.log("Connected to MongoDB.");
  })
  .catch(() => {
    console.log("Connection error.");
  });

const postRoutes = require('./routes/posts');
const userRoutes = require('./routes/user');

const app = express();

// BODY PARSER
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use("/images", express.static(path.join('backend/images')));

// CORS - Cross-Origin Resource Sharing
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Allow-Methods',
    'GET, POST, PUT, PATCH, DELETE, OPTIONS');
  next();
})

app.use('/api/posts', postRoutes);
app.use('/api/user', userRoutes);

module.exports = app;
