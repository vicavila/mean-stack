const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const userSchema = mongoose.Schema({
  // UNIQUE is not used to validate for unique value,
  // but is used for internal optimization done by mongodb
  email: {type: String, require: true, unique: true},
  password: {type: String, require: true},
  first_name: {type: String, require: true},
  last_name: {type: String, require: true}
});

userSchema.plugin(uniqueValidator); // this validates for existing email

module.exports = mongoose.model('User', userSchema);
