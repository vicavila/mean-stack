const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');


const router = express.Router();

router.post('/signup', (req, res, next) => {
  bcrypt.hash(req.body.password, 10, )
    .then(hash => {
      const user = new User({
        email: req.body.email,
        password: hash,
        last_name: req.body.lastName,
        first_name: req.body.firstName,
      });

      user.save().then(result => {
        res.status(201).json({message: 'User Created',
          user:{email: result.email, firstName: result.first_name, lastName: result.last_name}});
      }).catch(error => {
        res.status(500).json({error});
      });
    });
});

router.post('/login', (req, res, next) =>{

  let fetchUser;
  User.findOne({email: req.body.email})
    .then(user => {
      if(!user)
        return res.status(401).json({message: 'Login failed.'});

      fetchUser = user;
      return bcrypt.compare(req.body.password, user.password)
    }).then(result => {
      if(!result)
        return res.status(401).json({message: 'Login failed.'})

      const token = jwt.sign(
        {email: fetchUser.email, id: fetchUser._id},
        'secret_this_should_be_very_long_and_contains_special_characters',
        {expiresIn: '1h'} // EXPIRES IN 1HOURS
      );
      res.status(200).json({token: `Bearer ${token}`, expiresIn: 3600});
    }).catch(error => {
      res.status(401).json({message:'Login failed.'})
    });
})

module.exports = router;
