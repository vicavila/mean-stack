const express = require('express');
const multer = require('multer'); // github: multer for more info
const Post = require('../models/post'); // POST model
const authorization = require('../middleware/authorization');

const router = express.Router();

const MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg'
};

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error("Invalid mime type.");
    if(isValid)
      error = null;

    callback(error, "backend/images");
  },
  filename: (req, file, callback) => {
    const name = file.originalname.toLocaleLowerCase().split(' ').join('-');
    const ext = MIME_TYPE_MAP[file.mimetype];
    callback(null, name + "-" + Date.now() + '.' + ext);
  }
});


// ADD POST
router.post('', authorization,
  multer({storage: storage}).single("image"),
  (req, res, next) => {

  const url = req.protocol + '://' + req.get('host');
  const post = new Post({ // AUTO CREATE ID
    title: req.body.title,
    content: req.body.content,
    imagePath: url + "/images/" + req.file.filename
  });

  // SAVE TO MONGODB CLUSTER
  post.save().then(result => {
    res.status(201).json({
      message: "Post added successfully.",
      post: {
        id: result._id,
        ...result,
      }
    });
  });
});

// EDIT POST
router.put('/:id', authorization,
  multer({storage: storage}).single("image"), (req, res, next) => {

  let imagePath = req.body.imagePath;
  if(req.file) {
    const url = req.protocol + '://' + req.get('host');
    imagePath = url + "/images/" + req.file.filename;
  }

  const post = new Post({
    _id: req.params.id,
    title: req.body.title,
    content: req.body.content,
    imagePath: imagePath
  });

  Post.updateOne({_id: req.params.id}, post)
    .then(result => {
      console.log(result);
      res.status(200).json({message: "Update successful!"});
    })
    .catch(error => console.log(error));
})

// GET ALL POSTS
router.get('', (req, res, next) => {
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const postQuery = Post.find();
  let fetchedPosts;

  if(pageSize && currentPage) {
    postQuery.skip(pageSize * (currentPage - 1))
      .limit(pageSize);
  }
  postQuery.then(result => {
    const transformedPosts = result.map(post => {
      return {
        id: post._id,
        title: post.title,
        content: post.content,
        imagePath: post.imagePath
      };
    });
    fetchedPosts = transformedPosts;
    return Post.countDocuments();
  }).then(maxCount => {
    res.status(200).json({
      message: "Post fetch success,",
      posts: fetchedPosts,
      maxCount
    });
  });
});

//GET POST BY ID
router.get('/:id', (req, res, next) => {
  Post.findById({_id: req.params.id})
    .then(result => {
      if(result) {
        let post = {
          id: result._id,
          title: result.title,
          content: result.content,
          imagePath: result.imagePath
        };
        return res.status(200).json(post);
      }
      else
        return res.status(404).json({message: "Post not found."});
    });
})

// DELETE POST BY ID
router.delete('/:id', authorization, (req, res, next) => {
  Post.deleteOne({ _id: req.params.id})
    .then(result => {
      console.log(result);
      res.status(200).json({message: 'Post was deleted.'});
    });
});

module.exports = router;
