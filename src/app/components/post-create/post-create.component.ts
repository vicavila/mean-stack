import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/post';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {mimeType} from './mime-type.validator';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {
  enteredTotle = "";
  enteredContent = "";
  mode = "create";
  postId: string;
  post = {} as Post;
  isLoading = false;
  componentTitle = "Add New Post";
  postForm: FormGroup;
  imageUrl: string;

  constructor(private route: ActivatedRoute,
    private postService: PostService) { }


  ngOnInit(): void {
    this.postFormInit();

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('id')) {
        this.mode = "edit";
        this.postId = paramMap.get('id');
        this.getPost(this.postId);
        this.componentTitle = "Edit Post";
      }
      else {
        this.mode = "create";
        this.postId = null;
      }
    })
  }

  postFormInit() {
    this.postForm = new FormGroup({
      title : new FormControl(null,
        {validators: [Validators.required, Validators.minLength(3)]}),
      content : new FormControl(null,
        {validators: [Validators.required]}),
      image : new FormControl(null,
        {validators: [Validators.required],
        asyncValidators: [mimeType]}),
    });
  }

  getPost(id: string) {
    this.isLoading = true;
    this.postService.getPost(this.postId)
      .subscribe(postData => {
        this.post = postData;
        this.isLoading = false;
        this.postForm.setValue({
          title: this.post.title,
          content: this.post.content,
          image: this.post.imagePath
        })
      }, error => console.log(error));
  }

  onSavePost() {
    if(this.postForm.invalid)
      return;

    this.isLoading = true;
    if(this.mode === "create")
      this.postService.addPost(this.postForm.value.title,
        this.postForm.value.content, this.postForm.value.image);
    else
      this.postService.updatePost(
        this.postId, this.postForm.value.title,
        this.postForm.value.content, this.postForm.value.image);

    this.postForm.reset();
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.postForm.patchValue({image: file});
    this.postForm.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imageUrl = reader.result as string;
    };
    reader.readAsDataURL(file);
  }
}
