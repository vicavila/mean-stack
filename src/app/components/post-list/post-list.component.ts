import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from 'src/app/models/post';
import { Subscription } from 'rxjs';
import { PostService } from 'src/app/services/post.service';
import { PageEvent } from '@angular/material/paginator';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {

  posts : Post[] = [];
  isLoading = false;
  private postsSub: Subscription;
  totalPosts = 0;
  pageSize = 2;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];
  private authStatusSubs: Subscription;
  userIsAuthenticated = false;

  constructor(private postService: PostService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.getPosts();
    this.userIsAuthenticated = this.userService.getAuth();
    this.authStatusSubs = this.userService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      })
  }

  getPosts() {
    this.isLoading = true;
    this.postService.getPosts(this.pageSize, this.currentPage);
    this.postsSub = this.postService.getPostUpdateListener()
      .subscribe((postData: {posts: Post[], maxCount: number}) => {
        this.posts = postData.posts;
        this.totalPosts = postData.maxCount;
        this.isLoading = false;
      })
  }

  onDelete(id: string) {
    this.postService.deletePost(id)
      .subscribe(() => {
        this.postService.getPosts(this.pageSize, this.currentPage);
      }, error => console.log(error));
  }

  ngOnDestroy(): void {
    this.postsSub.unsubscribe();
    this.authStatusSubs.unsubscribe();
  }

  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.pageSize = pageData.pageSize;
    this.postService.getPosts(this.pageSize, this.currentPage);
  }
}
