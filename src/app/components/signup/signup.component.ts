import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {


  isLoading = false;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  onSignUp(form: NgForm) {
    if(form.invalid)
      return;

    this.isLoading = true;
    this.userService.createUser(form.value.email,form.value.password,
      form.value.lastName, form.value.firstName);
  }
}
