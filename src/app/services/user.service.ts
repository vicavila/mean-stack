import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthModel } from '../models/auth';
import { UserModel } from '../models/user';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userEndPoint = "http://localhost:3000/api/user";
  user = {} as UserModel;
  token: string;
  isAuthenticated = false;
  private tokenTimer: any;
  private authStatusListener = new Subject<boolean>();

  constructor(private http: HttpClient,
    private router: Router) { }

  getToken(){
    return this.token;
  }
  getAuth() {
    return this.isAuthenticated;
  }

  createUser(email: string, password: string, lastName: string, firstName: string) {
    const user: UserModel = {email, password, lastName, firstName};

    this.http.post(this.userEndPoint + "/signup", user)
      .subscribe((response: UserModel) => {
        this.user = response;
        this.router.navigate(['/login']);
      })
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  login(email: string, password: string){
    const authModel: AuthModel = {email, password};

    this.http.post<{token: string, expiresIn: number}>(this.userEndPoint + "/login", authModel)
      .subscribe(response => {
        this.token = response.token;
        if(this.token) {
          const expiresIn = response.expiresIn * 1000;
          this.setAuthTimer(expiresIn);

          this.isAuthenticated = true;
          this.authStatusListener.next(true);
          const expiration = new Date(new Date().getTime() + expiresIn);
          this.saveAuthData(this.token, expiration);
          this.router.navigate(['/posts']);
        }
      });
  }

  autoAuthUser() {
    const authInfo = this.getAuthData();
    if(!authInfo) return;

    const now = new Date();
    const expiresIn = authInfo.expiration.getTime() - now.getTime();
    if(expiresIn > 0) {
      this.setAuthTimer(expiresIn);
      this.token = authInfo.token;
      this.isAuthenticated = true;
      this.authStatusListener.next(true);
    }
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(['/posts']);
  }

  private setAuthTimer(duration) {
    console.log('Setting timer: ' + duration);
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration);
  }

  private saveAuthData(token: string, expirationDate: Date) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const expiration = localStorage.getItem('expiration');
    if(!token || !expiration)
      return;
    else
      return {token, expiration: new Date(expiration)};
  }
}
