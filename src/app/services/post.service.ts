import { Injectable } from '@angular/core';
import { Post } from '../models/post';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  posts: Post[] = [];
  private subjectPosts = new Subject<{posts: Post[], maxCount: number}>();

  postEndPoint = "http://localhost:3000/api/posts";
  constructor(private http: HttpClient,
    private router: Router) { }

  getPosts(pageSize: number, currentPage: number){
    const queryParams = `?pageSize=${pageSize}&page=${currentPage}`;

    this.http.get<{message: string, posts: Post[], maxCount: number}>
      (this.postEndPoint + queryParams)
        .subscribe((result: any) => {
          this.posts = result.posts;
          this.subjectPosts.next({posts: [...this.posts], maxCount: result.maxCount});
        });
  }

  getPostUpdateListener() {
    return this.subjectPosts.asObservable();
  }

  getPost(id: string) {
    return this.http.get<Post>(this.postEndPoint + "/" + id);
  }

  addPost(title: string, content: string, image: File) {
    // const post: Post = {id:null, title, content};
    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image, title);

    this.http.post<{message: string, post: Post}>(this.postEndPoint, postData)
      .subscribe(response => {
        this.router.navigate(['/posts']);
      })
  }

  updatePost(id: string, title: string, content: string, image: File | string) {
    let postData: Post | FormData;
    if(typeof(image) === 'object'){
      postData = new FormData();
      postData.append("id", id);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image, title);
    }
    else {
      postData = {id, title, content, imagePath: image};
    }

    this.http.put<Post>(this.postEndPoint + "/" + id, postData)
      .subscribe(response => {
        this.router.navigate(['/posts']);
      })
  }

  deletePost(id: string) {
    return this.http.delete(this.postEndPoint + "/" + id);
  }
}
