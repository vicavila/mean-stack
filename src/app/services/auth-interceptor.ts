import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserService } from './user.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{

  constructor(private userService: UserService){}

  intercept(request: HttpRequest<unknown>, next: HttpHandler) {
    const token = this.userService.getToken();

    if(token)
      request = request.clone(
        { headers: request.headers.set('Authorization', token) }
      );
    return next.handle(request);
  }
}
